package com.kerberos.authorization;

import javax.annotation.security.RolesAllowed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestAuthController {

	private static Logger log = LoggerFactory.getLogger(RestAuthController.class);
	
	public static class UserDetailsResponse{
		private String userId;
		private boolean authenticated;
		
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		
		public boolean isAuthenticated() {
			return authenticated;
		}
		public void setAuthenticated(boolean authenticated) {
			this.authenticated = authenticated;
		}
	}

	
	@GetMapping(value = "sso")
	@RolesAllowed("ROLE_USER")
	public ResponseEntity<UserDetailsResponse> sso() {
		 Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		log.info("Successfully accessed /sso url");
		UserDetailsResponse usd = new UserDetailsResponse();
		usd.setUserId(auth.getName());
		usd.setAuthenticated(auth.isAuthenticated());
		log.info("Successfully accessed /sso principal " + auth.getPrincipal());
		
		return ResponseEntity.ok(usd);
	}

}
