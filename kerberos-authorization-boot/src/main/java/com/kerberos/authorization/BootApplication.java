package com.kerberos.authorization;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootApplication {

	private static Logger log = LoggerFactory.getLogger(BootApplication.class);

	public static void main(String[] args) throws Throwable {
		log.info("Starting Boot application ");
		SpringApplication.run(BootApplication.class, args);
	}
}
