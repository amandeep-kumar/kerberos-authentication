package com.kerberos.authorization;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.kerberos.authentication.KerberosAuthenticationProvider;
import org.springframework.security.kerberos.authentication.KerberosServiceAuthenticationProvider;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosClient;
import org.springframework.security.kerberos.authentication.sun.SunJaasKerberosTicketValidator;
import org.springframework.security.kerberos.web.authentication.SpnegoAuthenticationProcessingFilter;
import org.springframework.security.kerberos.web.authentication.SpnegoEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private static Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);

	@Autowired
	private DummyUserDetailsService userService;

	@Value("${app.service-principal}")
	private String servicePrincipal;

	@Value("${app.keytab-location}")
	private String keytabLocation;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		/*http.exceptionHandling().authenticationEntryPoint(spnegoEntryPoint())
		        .and().authorizeRequests()
				.antMatchers("/", "/home").permitAll()
				.anyRequest().authenticated()
				.and().formLogin().loginPage("/login").permitAll()
				.and().logout().permitAll()
				.and()
				.addFilterBefore(spnegoAuthenticationProcessingFilter(authenticationManagerBean()),
						BasicAuthenticationFilter.class);
		*/
		http.exceptionHandling().authenticationEntryPoint(spnegoEntryPoint())
        .and().authorizeRequests().anyRequest().authenticated()
        .and()
		.addFilterBefore(spnegoAuthenticationProcessingFilter(authenticationManagerBean()),
				BasicAuthenticationFilter.class);
		

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(kerberosAuthenticationProvider())
				.authenticationProvider(kerberosServiceAuthenticationProvider());

	}

	@Bean
	public KerberosAuthenticationProvider kerberosAuthenticationProvider() {
		KerberosAuthenticationProvider provider = new KerberosAuthenticationProvider();
		SunJaasKerberosClient client = new SunJaasKerberosClient();
		client.setDebug(true);
		provider.setKerberosClient(client);
		provider.setUserDetailsService(userService);
		return provider;
	}

	@Bean
	public SpnegoEntryPoint spnegoEntryPoint() {
		//return new SpnegoEntryPoint("/login");
		//return new SpnegoEntryPoint("/");
		return new SpnegoEntryPoint();
	}

	@Bean
	public SpnegoAuthenticationProcessingFilter spnegoAuthenticationProcessingFilter(
			AuthenticationManager authenticationManager) {

		List<AuthenticationProvider> providerList = new ArrayList<>();
		providerList.add(kerberosServiceAuthenticationProvider());
		ProviderManager manager = new ProviderManager(providerList, authenticationManager);

		SpnegoAuthenticationProcessingFilter filter = new SpnegoAuthenticationProcessingFilter();
		filter.setAuthenticationManager(manager);

		return filter;
	}

	@Bean
	public KerberosServiceAuthenticationProvider kerberosServiceAuthenticationProvider() {
		KerberosServiceAuthenticationProvider provider = new KerberosServiceAuthenticationProvider();
		provider.setTicketValidator(sunJaasKerberosTicketValidator());
		provider.setUserDetailsService(userService);
		return provider;
	}

	@Bean
	public SunJaasKerberosTicketValidator sunJaasKerberosTicketValidator() {

		log.debug("Service Principal : " + servicePrincipal);
		log.debug("Keytab location : " + keytabLocation);

		SunJaasKerberosTicketValidator ticketValidator = new SunJaasKerberosTicketValidator();
		ticketValidator.setServicePrincipal(servicePrincipal);

		Resource keytabFile = null;
		if (keytabLocation.startsWith("classpath")) {
			String path = keytabLocation.replace("classpath:", "");
			log.info("class path resource :  " + path);
			keytabFile = new ClassPathResource(path);
		} else {
			keytabFile = new FileSystemResource(keytabLocation);
			log.info("file system resource :  " + keytabLocation);
		}

		ticketValidator.setKeyTabLocation(keytabFile);
		ticketValidator.setDebug(true);
		return ticketValidator;
	}

	/*
	 * public GlobalSunJaasKerberosConfig globalSunJaasKerberosConfig() {
	 * GlobalSunJaasKerberosConfig kerbConfig = new
	 * GlobalSunJaasKerberosConfig(); kerbConfig.setDebug(true);
	 * kerbConfig.setKrbConfLocation(krbIniLocation); return kerbConfig; }
	 */
}
